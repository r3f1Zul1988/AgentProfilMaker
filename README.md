# AgentProfilMaker

Splitt.py splittet Screenshots von Missionsreihen von Agentprofilen des Spieles Ingress in einzelne Grafiken

AgentProfilMaker.py Generiert aus einer Profil.png(welches den oberen Teil des Profiles mit den Badges darstellt), den zuvor mithilfe von Splitt.py erstellten Missionsreihen und ggf. den Stats des Agent eine Grafik.

##Voraussetzung 
     * Python3.5+
     * Python Imaging Library (PIL)

##Anleitung

###1. Screenshots machen
1. Screenshots von den Missionsreihen von der obersten Reihe beginnend machen
2. Keine Missionsreihen doppelt erfassen. Am besten immer 9 Reihen und die 10te als neuen Ausgangspunkt wählen

###2. Screenshots verarbeiten 
1. Screenshot mit den Badges des Agents manual mit einem Tool seiner Wahl zuschneiden. Die untere Kante muss dabei oberhalb der ersten Missionsreihe enden. Grafik unter input/<Agentname> als profil.png speichern
2. Falls man auch die Stats in die Grafik soll, muss man auch dieses aus einem Screenshot mit einem Tool seiner Wahl zuschneiden. Die obere Kannte sollte dabei der türkise Strich nach den Missionen sein. Grafik unter input/<Agentname> als stats.png speichern
3. Erstellte Screenshots der Missionsreihen in den Ordner tmp abgelegen
4. In Splitt.py Anpassungen vornehmen. User = Agentname und dirNumber beim erstenmal gleich 0. Wenn man das Profil aktualsieren möchte und vorherige Missionsreihen schon gesplittet in Missions für den Agent hinterlegt sind, muss dirNumber entsprechend der Ordneranzahl+1 des Missions Ordner von dem Agent angepasst werden.cropSize gibt den Wert wie groß der Bildausschnitt sein soll. Er sollte so gewählt werden das er eine Pixelzeile vor der nächsten Missionsreihe endet. Am besten vor dem erstenmal mit einem Screenshot testen welcher Wert bei euren Gerät optimal ist.
5. Danach Splitt.py in der Konsole ausführen: python3 splitt.py
6. Nach der Verarbeitung der Bilddaten liegen die Missionsreihen unter dem angegebenen Agentnamen im Missions-Ordner
7. Jetzt nur noch jeden Ordner überprüfen, dass jede Missionsreihe nur einmal vorhanden ist und die Bildausschnitte oberhalb der Missionsreihe beginnen und vor der nächsten Missionreihe enden. Alle anderen Bilddaten die Überlappungen etc. darstellen können gelöscht werden.

###3. Agentprofil generieren
1.  In AgentProfileMaker2.py Anpassungen vornehmen. User = Agentname und private = "true" falls keine stats.png in die neue Grafik soll ansonst "false"
2.  Danach AgentProfileMaker2.py in der Konsole ausführen: python3 AgentProfileMaker2.py
3.  Nach der Verarbeitung liegt die generierte Grafik unter dem angegebenen Agentnamen im Missions-Ordner
